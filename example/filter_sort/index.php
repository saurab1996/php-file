<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <title>Document</title>
</head>
<body>
    <div class="container">
    <h1 class="text-success text-uppercase text-center">filter sort operation</h1>
    <select id="select1" class="text-uppercase text-center form-select form-select-sm" aria-label=".form-select-sm example">
    <option val="5" selected>filter aggregate func</option>
  <option val="1">sum of salary</option>
  <option val="2">maximum salary</option>
  <option val="3">minimum salary</option>
  <option val="4">average salary</option>

   
</select> 
<select id="select2" class="text-uppercase text-center form-select form-select-sm" aria-label=".form-select-sm example">
  <option  selected>sort by</option>
  <option val="1">asce name</option>
  <option val="2">desc name</option>
  <option val="3">asce salary</option>
  <option val="4">desc salary</option>
  

</select>
<br>
<br>
      <div id="record_contant">
      
      </div>
    </div>


    <script type="text/javascript">
    $(document).ready(function() {

viewrecord();
});
        function viewrecord() {
            console.log(123);
        var viewrecords = 'viewrecords';
        $.ajax({
            url: 'server1.php',
            method: 'post',
            data: {
                viewrecords: viewrecords
            },
            success: function(data, status) {
                $('#record_contant').html(data);
            }

        });
    };
    $('#select1').change(function(){

      var selected1=$(this).val();
      console.log(selected1);
      if($(this).val()=='filter aggregate func')
      {
          viewrecord();
          $('#select2').prop("disabled",false);
      }
      else {
        $('#select2').prop("disabled",true);
      $.ajax({
         url:'server1.php',
         method:'post',
         data:{
            selected1:selected1
         },
         success:function(data)
         {
            $('#record_contant').html(data);   
            
         }


      });
      }
    })
    $('#select2').change(function(){
        var selected2=$(this).val();
        console.log(selected2);
        if(selected2=="sort by")
        {
            viewrecord();
        }
        else {
        $.ajax({
          url:'server1.php',
          method:'post',
          data:{
              selected2:selected2,
          },
          success:function(data)
          {
            $('#record_contant').html(data);
          }


        });

        }
    })
    
    </script>
</body>
</html>