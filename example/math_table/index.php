<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <title>math table </title>
</head>
<body>
     
     <div class="container">
     <br>
    <h1 class="text-success text-uppercase text-center">math table</h1>
    <div class="input-group rounded">
            <input type="text" class="form-control rounded" placeholder="enter number to get table of that number"  aria-label="Search"
                aria-describedby="search-addon" id="searchnum"/>  
        </div>
        <br>
        <div class=" text-center ">
            <button type="button" class="btn btn-primary text-uppercase text-center" id="gettable">
                get table
            </button>
        
        </div>
        <br>
       
         <div id="contant">
         
         </div>
     </div>
     <script type="text/javascript">
     
        $('#gettable').click(function(){ 
            var value=$('#searchnum').val();
             $.ajax({
                  url:'server.php',
                  method:'post',
                  data:{
                     value:value
                  },
                  success:function(data){

                      $('#contant').html(data);
                  }

             })
        });
        $('#searchnum').on("keyup",function()
        {
            var search_term=$(this).val();
            var search="search";
            if(search_term=="")
            {  
              $.ajax({

                  url:"server.php",
                  method:"post",
                  data:{
                     search:search
                  },
                  success:function(data)
                  {
                      $('#contant').html(data);
                  }
              })
                
            }
        });
     </script>
</body>
</html>