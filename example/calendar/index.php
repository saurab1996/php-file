<!DOCTYPE html>
<html>
    <head>
        <title>Calender</title>
        <script type="text/javascript" src="app.js"></script>
        <style>
            *{
                margin:0;
                padding: 0;
            }
            #forms{
                margin:25px;
                 }
            
            #BookF,#Month,#SDate,#ETime, #STime,#EDate{
                margin-top: 10px;
                height: 35px;
                color: black;
                text-align: center;
            }
            #btn{
                margin-top: 10px;
                height: 35px;
                width: 178px;
                text-align: center;
                font-size: 18;
            }
            #calen{
                border: 2px solid black;
                border-collapse: collapse;
            }
            #calen td{
                border: 1px solid;
                border-color: #000;
                padding: 5px;
            }
        </style>
        <body onload='table1()'>  
            <form id="forms">
                <input type="text" id="SDate" placeholder="Start Date: ">
                <input type="text" id="EDate" placeholder="End Date: "><br/>
                <input type="text" id="STime" placeholder="Start Time: ">
                <input type="text" id="ETime" placeholder="End Time: "><br/>
                <input type="text" id="BookF" placeholder="Book For: ">
                <br/>
                <input type="button" id="btn" value="Submit" onclick="Javascript:cal()">
             </form>
             <div id="calen">
                 
             </div>
        </body>
    </head>
</html>