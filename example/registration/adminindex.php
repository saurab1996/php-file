<?php
  session_start();

  if (!isset($_SESSION['username'])) {
      $_SESSION['msg'] = "You must log in first";
      header('location: adminlogin.php');
  }
  if (isset($_GET['logout'])) {
      session_destroy();
      unset($_SESSION['username']);
      header("location: adminlogin.php");
  }
?>
<!DOCTYPE html>
<html>

<head>
    <title>Home</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <style type="text/css">
    body {
        text-align: center;
    }

    form {
        display: inline-block;
    }
    </style>
</head>

<body>

    <div class="header">
        <h2>Home Page</h2>
    </div>
    <div class="content">
        <!-- notification message -->
        <?php if (isset($_SESSION['success'])) : ?>
        <div class="error success">
            <h3>
                <?php
              echo $_SESSION['success'];
              unset($_SESSION['success']);
          ?>
            </h3>
        </div>

        <?php endif ?>


        <!-- logged in user information -->
        <?php  if (isset($_SESSION['username'])) : ?>
        <p>Welcome <strong><?php echo $_SESSION['username']; ?></strong></p>
        <p> <a href="adminindex.php?logout='1'" style="color: red;">logout</a> </p>
        <?php endif ?>

        <br>
        <br>
        <div class="container">
            <div>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                    Open modal
                </button>

            </div>
            <h2 class="text-danger">All Records</h2>
            <div id="records_content">
            </div>
            <!-- The Modal -->
            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Add User </h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="form-group">

                                <div>
                                    <label>Username</label>
                                    <input type="text" value="" id="username" class="form-control">
                                </div>
                                <div>
                                    <label>Email</label>
                                    <input type="email" value="" id="email" class="form-control">
                                </div>
                                <div>
                                    <label>Password</label>
                                    <input type="password" id="password1" class="form-control">
                                </div>
                                <div>
                                    <label>Confirm password</label>
                                    <input type="password" id="password2" class="form-control">
                                </div>


                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"
                                onclick="addrecords()">submit</button>
                        </div>
                        <br>
                        <div>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        </div>
        <br>
        <button id="displaydata" class="btn btn-danger">Display all user </button>
        <div>
            <table class="table table-striped table-bordered text-center">
                <thead>
                    <th>Id</th>
                    <th>username</th>
                    <th>email</th>
                    <th>password</th>
                </thead>
                <tbody id="response">
                </tbody>
            </table>
        </div>
        <script type="text/javascript">
        function addrecords() {
            var username = $('#username').val();
            var email = $('#username').val();
            var password_1 = $('#password1').val();
            password_2 = $('#password2').val();
            $.ajax({
                url: 'server1.php',
                type: 'post',
                data: {
                    username: username,
                    email: email,
                    password_1: password_1,
                    password_2: password_2
                },
                success: function(data, status) {
                         $('#records_content').html(data);
                }

            });
        }
        $(document).ready(function() {
            $('#displaydata').click(function() {
                $.ajax({
                    url: 'ajaxresponse.php',
                    type: 'post',
                    success: function(getdata) {
                        $('#response').html(getdata);
                    }
                });
            });
        });
        </script>
</body>

</html>