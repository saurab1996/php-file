<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

abstract class person
{
    public $age;
    public $name;
    public function __construct($name,$age)
    {
        $this->age=$age;
        $this->name=$name;
    }
    abstract public function info_show();
}
class student extends person
{
    //function info_show()
    public function info_show()
    {   echo "<br>";
        
        echo "student name is ", $this-> name ,": age is", $this-> age ;
        
    }
}
class teacher extends person
{
    function info_show()
    {   
        echo "teacher name is ", $this-> name ,": age is", $this-> age ;
    }
}

$saurab=new student("saurab",24);
$prof_priya=new teacher("priya",35);
$prof_priya->info_show();
$saurab->info_show();
///abstract example
abstract class licenes{

    protected $id,$name;
    protected $issue_date;
    protected $expiry_date;
    protected $type;
    public function __construct( $name,$id,$issue_date,$expiry_date,$type)
    {   
        $this->name=$name;
        $this->id=$id;
        $this->issue_date=$issue_date;
        $this->expiry_date=$expiry_date;
        $this->type=$type;
    }
    abstract function Showdata();

}
class two_wheeler extends licenes
{
    function showdata()
    {
       echo " name of vehicle is  $this->name ";
       echo "<br> ID of this vehicle is $this->id <br>";
    }
}
$passion_pro=new two_wheeler("hero passion pro",10,"10-may-2012","10-12-2024","two-wheeler");
$passion_pro->showdata();
?>
