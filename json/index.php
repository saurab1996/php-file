<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        #chartdiv {
          width: 100%;
          height: 500px;
        }
        
        </style>
        
        <!-- Resources -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="core.js"></script>
        <script src="charts.js"></script>
        <script src="animated.js"></script>
    <title>Document</title>
</head>
<body>
    <div id="chartdiv"></div>
    <script>
      $.getJSON('http://json.test/pie.json', function(data1){
        console.log(data1);
      });
    
    am4core.ready(function() {
    
    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end
    
    var chart = am4core.create("chartdiv", am4charts.PieChart3D);
    chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
    
    chart.legend = new am4charts.Legend();
    
    chart.data = [
      {
        country: "Lithuania",
        litres: 501.9
      },
      {
        country: "Czech Republic",
        litres: 301.9
      },
      {
        country: "Ireland",
        litres: 201.1
      },
      {
        country: "Germany",
        litres: 165.8
      },
      {
        country: "Australia",
        litres: 139.9
      },
      {
        country: "Austria",
        litres: 128.3
      },
      {
        country: "UK",
        litres: 99
      },
      {
        country: "Belgium",
        litres: 60
      },
      {
        country: "The Netherlands",
        litres: 50
      }
    ];
    
    chart.innerRadius = 100;
    
    var series = chart.series.push(new am4charts.PieSeries3D());
    series.dataFields.value = "litres";
    series.dataFields.category = "country";
    
    }); // end am4core.ready()
    </script>
    
    <!-- HTML -->
</body>
</html>