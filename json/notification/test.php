<?php
//$apnsHost='api.push.apple.com';
// $apnsHost = 'gateway.sandbox.push.apple.com';
// $apnsCert = 'vibe.pem';
// $apnsPort = 2195;

// $streamContext = stream_context_create();
// stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);

// $apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);

// $payload['aps'] = array('alert' => 'Oh hai Swaroop!', 'badge' => 1, 'sound' => 'default');
// $output = json_encode($payload);
// $token =  'ff639e08028c79500f6f02fc3e89e871a7e9704bb5cd84c3acf6b590a2135f89';
// $token = pack('H*', str_replace(' ', '', $token));
// $apnsMessage = chr(0) . chr(0) . chr(32) . $token . chr(0) . chr(strlen($output)) . $output;
// $result = fwrite($apns, $apnsMessage);

// if (!$result)
//     echo 'Message not delivered' . PHP_EOL;
// else
//     echo 'Message successfully delivered' . PHP_EOL;
// socket_close($apns);
// fclose($apns);

require __DIR__ . '/vendor/autoload.php';

use Pushok\AuthProvider;
use Pushok\Client;
use Pushok\Notification;
use Pushok\Payload;
use Pushok\Payload\Alert;

$options = [
    'app_bundle_id' => 'com.vinay.vibe', // The bundle ID for app obtained from Apple developer account
    'certificate_path' => 'vibe.pem', // Path to private key
    'certificate_secret' => null // Private key secret
];

// Be aware of thing that Token will stale after one hour, so you should generate it again.
// Can be useful when trying to send pushes during long-running tasks
$authProvider = AuthProvider\Certificate::create($options);

$alert = Alert::create()->setTitle('Hello!');
$alert = $alert->setBody('First push notification');

$payload = Payload::create()->setAlert($alert);

//set notification sound to default
$payload->setSound('default');

//add custom value to your notification, needs to be customized
$payload->setCustomValue('key', 'value');

$deviceTokens = ['ff639e08028c79500f6f02fc3e89e871a7e9704bb5cd84c3acf6b590a2135f89',];
$notifications = [];
foreach ($deviceTokens as $deviceToken) {
    $notifications[] = new Notification($payload,$deviceToken);
}

// If you have issues with ssl-verification, you can temporarily disable it. Please see attached note.
// Disable ssl verification
//  $client = new Client($authProvider, $production = false, [CURLOPT_SSL_VERIFYPEER=>false] );
$client = new Client($authProvider, $production = false);
$client->addNotifications($notifications);



$responses = $client->push(); // returns an array of ApnsResponseInterface (one Response per Notification)
foreach ($responses as $response) {
    // The device token
    $response->getDeviceToken();
    // A canonical UUID that is the unique ID for the notification. E.g. 123e4567-e89b-12d3-a456-4266554400a0
    $response->getApnsId();
    
    // Status code. E.g. 200 (Success), 410 (The device token is no longer active for the topic.)
    $response->getStatusCode();
    // E.g. The device token is no longer active for the topic.
    $response->getReasonPhrase();
    // E.g. Unregistered
    $response->getErrorReason();
    // E.g. The device token is inactive for the specified topic.
    $response->getErrorDescription();
    $response->get410Timestamp();
}

