<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'saurab/notification',
  ),
  'versions' => 
  array (
    'brick/math' => 
    array (
      'pretty_version' => '0.9.2',
      'version' => '0.9.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dff976c2f3487d42c1db75a3b180e2b9f0e72ce0',
    ),
    'edamov/pushok' => 
    array (
      'pretty_version' => '0.13.0',
      'version' => '0.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '743397b7590867f4d6396af6562edb3ca664f9b9',
    ),
    'fgrosse/phpasn1' => 
    array (
      'pretty_version' => 'v2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '20299033c35f4300eb656e7e8e88cf52d1d6694e',
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'saurab/notification' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'spomky-labs/base64url' => 
    array (
      'pretty_version' => 'v2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '7752ce931ec285da4ed1f4c5aa27e45e097be61d',
    ),
    'web-token/jwt-core' => 
    array (
      'pretty_version' => 'v2.2.10',
      'version' => '2.2.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '53beb6f6c1eec4fa93c1c3e5d9e5701e71fa1678',
    ),
    'web-token/jwt-key-mgmt' => 
    array (
      'pretty_version' => 'v2.2.10',
      'version' => '2.2.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '0b116379515700d237b4e5de86879078ccb09d8a',
    ),
    'web-token/jwt-signature' => 
    array (
      'pretty_version' => 'v2.2.10',
      'version' => '2.2.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '015b59aaf3b6e8fb9f5bd1338845b7464c7d8103',
    ),
    'web-token/jwt-signature-algorithm-ecdsa' => 
    array (
      'pretty_version' => 'v2.2.10',
      'version' => '2.2.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '44cbbb4374c51f1cf48b82ae761efbf24e1a8591',
    ),
  ),
);
