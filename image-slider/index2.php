<?php
$poke_url = 'https://pokeapi.co/api/v2/pokemon?offset=0&limit=01';
$json_data = file_get_contents($poke_url);
$response_data = json_decode($json_data);
$poke_data = $response_data->results;
$poke_ability=array();
$imgarray=array();
foreach ($poke_data as $pokemon){
    echo "Pokemon Name : " . $pokemon->name;
    echo "<br />";
    $pokemon_url = $pokemon->url;
    $json_data1 = file_get_contents($pokemon_url);
    $response_data1 = json_decode($json_data1);
    //print_r($response_data1);
    $poke_bility_data1 = $response_data1->abilities;
    echo "Abilities of Pokemon is :" ;
    foreach ($poke_bility_data1 as $pokemon1) {
        $ability_data = $pokemon1->ability;
        array_push($poke_ability,$ability_data);
        echo "</br>" . $ability_data->name;
    }
        
        $poke_mage = $response_data1->sprites->other;
        $str=$poke_mage->{'official-artwork'}->front_default;
        print_r($str);
        $img=file_get_contents($str);
  //      echo $img;
        array_push($imgarray,$str);
}

//print_r($imgarray)
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/bookmarks/fontawesome-free-5.14.0-web/css/all.css">
    <link rel="stylesheet" href="style.css">
    <title>Full SCREEN Slider</title>
    <style>
        .slide:first-child {
    background: url("<?php echo $imgarray[0]; ?>") no-repeat
      center center/cover;
  }
  .slide:nth-child(2){
    background: url('images/1.jpg') no-repeat
      center top/cover;
  }
  .slide:nth-child(3){
    background: url('images/3.jpg') no-repeat
      center top/cover;
  }
  .slide:nth-child(4){
    background: url('images/4.jpg') no-repeat
      center top/cover;
  }
  .slide:nth-child(5){
    background: url('images/5.jpg') no-repeat
      center top/cover;
  }
  .slide:nth-child(6){
    background: url('images/6.jpg') no-repeat
      center top/cover;
  }
    </style>

</head>
<div class="slider">
        <div class="slide current">
            <div class="content">
                <h1>Slide One</h1>
                <p><i class="fas fa-quote-left"></i> Lorem ipsum dolor sit amet consectetur
                    adipisicing elit. Debitis asperiores eaque
                    Provident est hic placeat id. <i class="fas fa-quote-right"></i></p>
            </div>
        </div>
        <div class="slide">
            <div class="content">
                <h1>Slide Two</h1>
                <p>Lorem ipsum dolor sit amet consectetur
                    adipisicing elit. Debitis asperiores eaque
                    Provident est hic placeat id.</p>
            </div>
            <footer class="footer"></footer>
        </div>
        <div class="slide">
            <div class="content">
                <h1>Slide Three</h1>
                <p>Lorem ipsum dolor sit amet consectetur
                    adipisicing elit. Debitis asperiores eaque
                    Provident est hic placeat id.</p>
            </div>
        </div>
        <div class="slide">
            <div class="content">
                <h1>Slide Four</h1>
                <p>Lorem ipsum dolor sit amet consectetur
                    adipisicing elit. Debitis asperiores eaque
                    Provident est hic placeat id.</p>
            </div>
        </div>
        <div class="slide">
            <div class="content">
                <h1>Slide five</h1>
                <p>Lorem ipsum dolor sit amet consectetur
                    adipisicing elit. Debitis asperiores eaque

                    Provident est hic placeat id.</p>
            </div>
        </div>
        <div class="slide">
            <div class="content">
                <h1>Slide Six</h1>
                <p>Lorem ipsum dolor sit amet consectetur
                    adipisicing elit. Debitis asperiores eaque

                    Provident est hic placeat id.</p>
            </div>
        </div>     adipisicing elit. Debitis asperiores eaque

                    Provident est hic placeat id.</p>
            </div>
        </div>
    </div>
    <div class="button">
        <button id="prev"><i class="fas fa-arrow-left"></i></button>
        <button id="next"><i class="fas fa-arrow-right"></i></button>
    </div>
    <script src="main.js"></script>
    <script src="main.js"></script>
</body>

</html>