<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Game</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<style>
* {
  box-sizing: border-box;
}

.column {
  float: left;
  width: 33.33%;
  padding: 5px;
}

img{
    border:5px solid #fd7e14;
}
/* Clearfix (clear floats) */
.row::after {
  content: "";
  clear: both;
  display: table;
}
.btn {

  background-color: #555;
  color: white;
  font-size: 16px;
  padding: 12px 24px;
  border: none;
  cursor: pointer;
  border-radius: 5px;

}
.btn1{


    background-color: #1a5d5c;
  color: white;
  font-size: 16px;
  padding: 12px 24px;
  border: none;
  cursor: pointer;
  border-radius: 5px;
}
.btn1:hover {
  background-color: black;

}
.column1 {
  float: left;
  width: 50%;
  padding: 5px;
}
.point{
    border:black;

}


</style>
</head>
<body>

<div class="container">
    
    <h1 class="text-primary text-uppercase text-center">Water snake and gun game</h1>
    
<div class="row">
  <div class="column">
    <button class="btn1" id="btn1" data-toggle="modal" data-target="#myModal">
                <img src="gun.jpeg" alt="Gun" style="width:300px;height:200px " >
    <span class="text-uppercase text-center badge badge-success" style="width:300px;border:5px solid #555">Choose Gun</span></button>
  </div>
  <div class="column" >
  <button class="btn1" id="btn2" data-toggle="modal" data-target="#myModal">
  <img src="snake.jpg" alt="snake" style="width:300px;height:200px " >
  <span class="text-uppercase text-center badge badge-success" style="width:300px;border:5px solid #555 " >Choose Snake</span>
  </button>

  </div>
  <div class="column">
  <button class="btn1" id="btn3" data-toggle="modal"  data-target="#myModal">
                <img src="water.jpeg" alt="water"style="width:300px;height:200px " >
  <span class="text-uppercase text-center badge badge-success"style="width:300px;border:5px solid #555">Choose Water</span>
  </button>
  </div>
</div>
<div class="modal" id="myModal">
  <div class="modal-dialog mw-100 w-75" >
    <div class="modal-content">

    

      <!-- Modal body -->
      <div class="modal-body" id="contant">
    
      </div>

      <!-- Modal footer -->
      

    </div>
  </div>
  
</div>
<hr> 

</div>
<div class="container">
<h2 class="text-dark text-uppercase text-center">point table</h2>
<div class="row">
  <div class="column">
  <img class="point" src="person.png" alt="water"style="width:300px;height:200px " >
  </div>
  <div class="column">
  <br><br><br>
  <span class="badge badge-pill badge-secondary">
    human:-  
   <span class="badge badge-light" id="point"> </span>
   </span>
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   &nbsp;
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   &nbsp;
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   &nbsp;
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   &nbsp;
   <span class="badge badge-pill badge-secondary">
   computer:- <span class="badge badge-light" id="point1"></span> </span>
  </div>
   <div class="column ">
   <img class="point" src="computer.png" alt="water"style="width:300px;height:200px " >
   </div>
  </div>
</div>
<script type="text/javascript">
   $('#btn1').on('click',function(){

     console.log(123);
     var btn1="btn1";
     $.ajax({
       url:"server.php",
       method:'post',
       data:{
           btn1:btn1
       },
       success:function(data){
           $('#contant').html(data);
       }
       
     });

   })

   $('#btn2').on('click',function(){

console.log(123);
var btn2="btn2";
$.ajax({
  url:"server.php",
  method:'post',
  data:{
      btn2:btn2
  },
  success:function(data){
      $('#contant').html(data);
  }
  
});

})
$('#btn3').on('click',function(){

console.log(123);
var btn3="btn3";
$.ajax({
  url:"server.php",
  method:'post',
  data:{
      btn3:btn3
  },
  success:function(data){
      $('#contant').html(data);
  }
  
});

})
function increasepoint()
{
    var point="point";
    $.ajax({
      url:"server.php",
      method:"post",
      data:{
       point:point
      },
      success:function(data)
      {  var value=JSON.parse(data);
      console.log(value);
         $('#point').html(value[0]);
         $('#point1').html(value[1]);
      }

    })
}
</script>
</body>
</html>
