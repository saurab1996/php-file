<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Document</title>
</head>
<body>
<div style="padding-left: 50px;padding-right: 50px;padding-top:100px;" class="container">
    <h2 class="text-center">HTML Table</h2>

    <table class="table table-dark table-striped table-hover">
      <tbody class=" tbl_code_with_mark">
      <tr >
        <th>Languages</th>
        <th>Expert Names</th>
        <th>Country</th>
      </tr>
      <tr>
        <td>HTML</td>
        <td>Code With Mark</td>
        <td>USA</td>
      </tr>
      <tr>
        <td>CSS</td>
        <td>Francisco Chang</td>
        <td>Mexico</td>
      </tr>
      <tr>
        <td>Javascript</td>
        <td>Roland Mendel</td>
        <td>Austria</td>
      </tr>
      <tr>
        <td>jQuery</td>
        <td>Helen Bennett</td>
        <td>UK</td>
      </tr>
      <tr>
        <td>PHP</td>
        <td>Yoshi Tannamuri</td>
        <td>Canada</td>
      </tr>
      <tr>
      <th>
      <label for=""> site</label>
      </th>
        <td><input type="text" name="" id="" value="heeloo"></td>
      
      </tr>
      </tbody>
    </table>

    <div class="text-center">
      <span class="btn btn-sm btn-primary btn_row_add_below_end"> Clone Last Row And Add At End</span>
    </div>
  </div>
  <script>
  $(document).ready(function($)
  {
    //--->add row at the end > start
    $(document).on('click',".btn_row_add_below_end", function(e)
    {
      var tableBody = $(document).find('.tbl_code_with_mark');
      //console.log(tableBody);
      var trLast = tableBody.find("tr:first");
      var trNew = trLast.clone();
      trNew.find('input:text').val('');
      tableBody.find("tr:last").after(trNew);
    });
  });
</script>
</body>
</html>