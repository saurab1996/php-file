<?php ?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>

    <div class="container">
        <h1 class="text-primary text-uppercase text-center">event create and handle</h1>
        
        <div class="text-left" >
            <input type="number" placeholder="startdate" id="startdate">
            
        </div>
        <br>
        <br>
        <div class="text-left">
        <input type="number" placeholder="enddate" id="enddate">
        </div>
        <br>
        <div class="text-uppercase text-left ">
            <button type="button" class="btn btn-primary text-uppercase text-center" id="addevent">
                CREATE EVENT CALENDAR
            </button>

            <button type="button" class="btn btn-primary text-uppercase text-center" data-toggle="modal"
                data-target="#myModal">
                add event
            </button>
        </div>
        <!-- The Modal -->
        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">fill detail </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <br>
                    <div class="text-center" >
            <input type="text" placeholder="teamname" id="teamname">
            </div>
            
            <br>
        <div class="text-center" >
            <input type="number" placeholder="startdate" id="eventstartdate">
            <input type="number" placeholder="starttime" id="eventstarttime">
        </div>
        <br>
        <br>
        <div class="text-center">
        <input type="number" placeholder="enddate" id="eventenddate">
        <input type="number" placeholder="endtime" id="eventendtime">
        </div>

                    <!-- Modal footer -->
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-danger " data-dismiss="modal"
                            onclick="addevent()">save event</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

                    </div>

                </div>
            </div>
        </div>

        <br>
        <div id="content">
        </div>
    </div>

    <script type="text/javascript">
      $('#addevent').on('click',function(){

      var startdate=$('#startdate').val();
      var enddate=$('#enddate').val();
       console.log(enddate,startdate);
       $.ajax({
       url:'server.php',
       type:'post',
       data:{
           startdate:startdate,
           enddate:enddate
       },
       success:function(data){
           $('#content').html(data);

       }

       })

      });
      function addevent(){
          let eventstarttime=$('#eventstarttime').val();  
          let eventendtime=$('#eventendtime').val();
          let eventstartdate=$('#eventstartdate').val();
          let eventenddate=$('#eventenddate').val();
          let teamname=$('#teamname').val();
          $.ajax({
              url:'addevent.php',
              type:'post',
              data:{
                  teamname:teamname,
                  starttime:eventstarttime,
                  endtime:eventendtime,
                  startdate:eventstartdate,
                  enddate:eventenddate
                },
                success:function(data)
                {
                  console.log(eventstartdate,eventstarttime,eventenddate,eventendtime,teamname);
                $('#content').html(data); 
              }
          });
      };
    </script>

</body>

</html>